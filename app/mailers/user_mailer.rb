# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def welcome_notification(user)
    @user = user
    @support_email = 'norad-support@cisco.com'
    @support_url = 'https://norad-gitlab.cisco.com/norad/support'
    @docs_url = 'https://norad.gitlab.io/docs/'
    @mailinglist_name = 'norad-users'
    @mailinglist_url = mailinglist_url(@mailinglist_name)

    mail to: @user.email, subject: 'Welcome to Norad'
  end

  def email_confirmation(user)
    @user = user
    token = @user.local_authentication_record.email_confirmation_token
    @email_confirmation_url = "#{url_for(controller: 'v1/email_confirmations', action: :create)}/#{token}"
    mail to: @user.email, subject: 'Please confirm your email address'
  end

  def password_reset(user)
    @user = user
    # If somehow a user authed via SSO manages to initiate a password reset, this will fail and we'll know about it
    token = @user.local_authentication_record.password_reset_token
    @password_reset_url = "#{v1_password_reset_url(token)}/edit"
    mail to: @user.email, subject: 'Norad password reset'
  end

  private

  def mailinglist_url(name)
    "https://wwwin-tools.cisco.com/itsm/mailer/listDetails.do?list=#{name}"
  end
end
