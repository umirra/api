# frozen_string_literal: true

module V1
  class InfosecExportQueuesController < ResultExportQueuesController
    private

    def result_export_queue_params
      params.require(:infosec_export_queue).permit(:auto_sync)
    end

    def child_create
      infosec_export_queue = InfosecExportQueue.new(result_export_queue_creation_params)

      if infosec_export_queue.save
        render json: infosec_export_queue, status: :created
      else
        render_errors_for(infosec_export_queue)
      end
    end
  end
end
