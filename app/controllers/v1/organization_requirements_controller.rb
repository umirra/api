# frozen_string_literal: true

module V1
  class OrganizationRequirementsController < V1::ApplicationController
    def index
      org = Organization.find(params[:organization_id])
      # Enforce read privileges on the organization
      authorize_action_for org
      render json: org.requirements.order(:name)
    end
  end
end
