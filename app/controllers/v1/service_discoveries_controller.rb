# frozen_string_literal: true

module V1
  class ServiceDiscoveriesController < ApplicationController
    before_action :set_machine, only: %i[index create]
    before_action :set_service_discovery, only: %i[show update]
    skip_before_action :require_api_token, only: :update
    before_action :require_signature, only: :update

    def index
      authorize_action_for ServiceDiscovery, in: @machine.organization
      render json: @machine.service_discoveries.limit(10)
    end

    def show
      authorize_action_for @service_discovery
      render json: @service_discovery
    end

    def create
      authorize_action_for ServiceDiscovery, in: @machine.organization
      service_discovery = ServiceDiscovery.new(machine: @machine)
      if service_discovery.save
        render json: service_discovery
      else
        render_errors_for(service_discover)
      end
    end

    def update
      service_discovery_params[:error_message].blank? ? @service_discovery.complete : @service_discovery.fail
      @service_discovery.error_message = service_discovery_params[:error_message]
      if @service_discovery.save
        render json: @service_discovery
      else
        render_errors_for(@service_discovery)
      end
    end

    private

    def service_discovery_params
      params.require(:service_discovery).permit(:error_message)
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def set_service_discovery
      @service_discovery = ServiceDiscovery.find(params[:id])
    end

    def require_signature
      require_container_secret_signature(@service_discovery.shared_secret)
    end
  end
end
