# frozen_string_literal: true

module V1
  class SecurityContainersController < ApplicationController
    before_action :set_security_container, only: :show

    def index
      render json: SecurityContainer.all
    end

    def show
      render json: @security_container
    end

    private

    def set_security_container
      @security_container = SecurityContainer.find(params[:id])
    end

    def security_container_params
      params.require(:security_container).permit(:name, :options)
    end
  end
end
