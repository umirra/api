# frozen_string_literal: true

module V1
  class MembershipsController < V1::ApplicationController
    before_action :set_organization, only: %i[index create]
    before_action :set_user, only: [:create]
    before_action :set_membership, only: [:destroy]

    def index
      authorize_action_for Membership, in: @organization
      render json: @organization.memberships
    end

    def create
      authorize_action_for Membership, in: @organization
      membership = if membership_params[:admin] =~ /\Atrue\z/i
                     Membership.create_admin!(@user, @organization)
                   else
                     Membership.create_reader!(@user, @organization)
                   end
      render json: membership
    rescue ActiveRecord::RecordInvalid
      render_errors_for(membership)
    end

    def destroy
      authorize_action_for @membership
      current_user != @membership.user ? @membership.destroy_with_roles! : nil

      head :no_content
    end

    def set_membership
      @membership = Membership.find(params[:id])
    end

    def set_organization
      @organization = Organization.find(params[:organization_id])
    end

    def set_user
      @user = User.find_by!(uid: membership_params[:uid])
    end

    def membership_params
      params.require(:membership).permit(:uid, :admin)
    end
  end
end
