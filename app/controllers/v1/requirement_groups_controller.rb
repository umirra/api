# frozen_string_literal: true

module V1
  class RequirementGroupsController < V1::ApplicationController
    authorize_actions_for RequirementGroup, only: %i[index show create]
    before_action :set_group, only: %i[show update]

    def index
      render json: requirement_groups
    end

    def show
      render json: @group
    end

    def create
      @req_group = RequirementGroup.new(group_params)
      ActiveRecord::Base.transaction do
        @req_group.save!
        current_user.add_role :requirement_group_admin, @req_group
      end
      render json: @req_group, serializer: RequirementGroupSerializer
    rescue ActiveRecord::RecordInvalid
      render_errors_for(@req_group)
    end

    def update
      if @group.update(group_params)
        render json: @group
      else
        render_errors_for(@group)
      end
    end

    private

    def group_params
      params.require(:requirement_group).permit(:name, :description)
    end

    def set_group
      @group = RequirementGroup.find(params[:id])
      authorize_action_for @group
    end

    def requirement_groups
      params[:with_admin].blank? ? RequirementGroup.all : RequirementGroup.with_role(:requirement_group_admin)
    end
  end
end
