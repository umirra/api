# frozen_string_literal: true

module V1
  class OrganizationsController < V1::ApplicationController
    before_action :set_organization, only: %i[show update destroy]
    authorize_actions_for Organization, only: :create

    def index
      allowed_roles = %i[organization_admin organization_reader]
      render json: Organization.with_roles(allowed_roles, current_user)
    end

    def show
      render json: @organization, include_assessment_summary: true
    end

    def create
      @organization = Organization.new(organization_params)
      create_organization_and_grant_admin
      render json: @organization
    rescue ActiveRecord::RecordInvalid
      render_errors_for(@organization)
    rescue ActiveRecord::RecordNotUnique
      @organization.errors.add :name, 'already exists in the database'
      render_errors_for(@organization)
    end

    def update
      if @organization.update(organization_params)
        render json: @organization
      else
        render_errors_for(@organization)
      end
    rescue ActiveRecord::RecordNotUnique
      @organization.errors.add :name, 'already exists in the database'
      render_errors_for(@organization)
    end

    def destroy
      @organization.destroy!
      head :no_content
    end

    private

    def set_organization
      @organization = Organization.find(params[:id])
      authorize_action_for @organization
    end

    def organization_params
      params.require(:organization).permit(:uid)
    end

    def create_organization_and_grant_admin
      ActiveRecord::Base.transaction do
        @organization.save!
        Membership.create_admin!(current_user, @organization)
      end
    end
  end
end
