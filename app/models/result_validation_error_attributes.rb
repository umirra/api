# frozen_string_literal: true

# This is a specific instance of a template following the template design
# pattern. See the base class ResultAbstractErrorAttributes for details.

class ResultValidationErrorAttributes < ResultAbstractErrorAttributes
  private

  def generate_title
    'Validation Criterion Not Satisified'
  end

  def generate_description(exception)
    "Security test post values not valid: #{exception.message}"
  end

  def generate_nid
    'validation-error:10010'
  end
end
