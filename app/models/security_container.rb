# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_containers
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  category               :integer          default("whitebox"), not null
#  prog_args              :string           not null
#  default_config         :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  multi_host             :boolean          default(FALSE), not null
#  test_types             :string           default([]), not null, is an Array
#  configurable           :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  help_url               :string
#
# Indexes
#
#  index_security_containers_on_common_service_type_id  (common_service_type_id)
#  index_security_containers_on_name                    (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (common_service_type_id => common_service_types.id)
#
# rubocop:enable Metrics/LineLength

class SecurityContainer < ApplicationRecord
  # Attribute Information
  enum category: %i[whitebox blackbox]

  TARGET_PARAM = '%{target}'
  SSH_USER_PARAM = '%{ssh_user}'
  SSH_KEY_PARAM = '%{ssh_key}'
  TARGET_SHIM = { target: 'validation placeholder' }.freeze
  SSH_SHIM = { ssh_key: 'abcdef', ssh_user: 'user' }.freeze
  SPACE_PLACEHOLDER = '~!_NORAD_SPACE_PLACEHOLDER_!~'
  TEST_TYPES = %w[authenticated web_application brute_force ssl_crypto ssh_crypto whole_host].sort

  # Validations
  validates :name,
            presence: true,
            format: {
              with: %r{\A[A-Za-z][\w:./-]+\z},
              message: 'can only contain alphanumeric, colons, periods, underscores and dashes'
            }
  validates :prog_args, presence: true
  validates :test_types, presence: true
  validate :program_args_specifies_target_parameter
  validate :white_box_containers_specify_ssh_parameters, if: :whitebox?
  validate :test_types_are_valid
  validate :whole_host_is_mutually_exclusive
  validate :test_types_is_not_empty
  validate :default_config_is_a_hash

  # Associations
  has_many :assessments, inverse_of: :security_container
  has_many :security_container_configs, inverse_of: :security_container
  has_many :provisions, inverse_of: :security_container
  has_many :requirements, through: :provisions, inverse_of: :security_containers
  belongs_to :common_service_type, inverse_of: :security_containers

  # Callbacks
  before_validation do |sc|
    test_types && sc.test_types.uniq!
    self.default_config ||= {}
  end

  # Miscellaneous Instance Methods
  def args_hash
    default_config.each_with_object({}) do |(k, v), h|
      h[k.to_sym] = v.to_s.gsub(/ /, SPACE_PLACEHOLDER)
    end
  end

  def config_for_machine(id)
    security_container_configs.for_machine(id)
  end

  def config_for_organization(id)
    security_container_configs.for_organization(id)
  end

  # Define methods like #whole_host? and #authenticated?
  TEST_TYPES.each do |ttype|
    define_method :"#{ttype}?" do
      test_types.include? ttype
    end
  end

  private

  # Validation Methods
  def program_args_specifies_target_parameter
    if prog_args && prog_args.scan(TARGET_PARAM).length.zero?
      errors.add(:prog_args, 'must specify target parameter')
    end
    true
  end

  def white_box_containers_specify_ssh_parameters
    if prog_args && prog_args.scan(SSH_USER_PARAM).length != 1 && prog_args.scan(SSH_KEY_PARAM).length != 1
      errors.add(:prog_args, 'must specify ssh parameters')
    end
    true
  end

  def prog_args_contains_parameters?
    return false unless prog_args
    # Another validation ensures that the target parameter is present
    prog_args.scan(/%{\w+}/).length > 1
  end

  def test_types_are_valid
    return unless test_types&.any? { |t| !TEST_TYPES.include?(t) }
    errors.add(:test_types, "must only contain the following: #{TEST_TYPES.join(', ')}")
  end

  def whole_host_is_mutually_exclusive
    return unless test_types&.include?('whole_host') && test_types.length > 1
    errors.add(:test_types, 'can only contain the whole_host option if it is specified')
  end

  def test_types_is_not_empty
    errors.add(:test_types, 'must specify at least one type') if test_types&.empty?
  end

  def default_config_is_a_hash
    errors.add(:default_config, 'must be a Hash') unless default_config.is_a?(Hash)
  end
end
