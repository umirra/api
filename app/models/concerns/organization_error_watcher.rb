# frozen_string_literal: true

module OrganizationErrorWatcher
  extend ActiveSupport::Concern

  included do
    after_save :check_for_organization_errors
    after_destroy :check_for_organization_errors
  end

  module ClassMethods
    def watch_for_organization_errors(*error_klasses, **options)
      # Some instances do not have an #organization method defined
      organization_method = options.fetch(:organization_method, :organization)
      # Users of this library can also define a custom version of this method if needed
      define_method :check_for_organization_errors do
        error_klasses.each do |klass|
          klass.send(:check, send(organization_method))
        end
      end

      private :check_for_organization_errors
    end
  end
end
