# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  containers            :json
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class DockerCommand < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include DockerCommandStateMachine

  # Associations
  acts_as_poly :commandable, :machine, :organization, inverse_of: :docker_commands
  has_many :assessments, inverse_of: :docker_command

  # Validations
  validates :machine, presence: true, if: proc { |a| a.organization.blank? }
  validates :organization, presence: true, if: proc { |a| a.machine.blank? }
  validate :containers_is_an_array_with_real_container_names

  # Callback Declarations
  before_destroy :check_if_scan_complete
  before_validation :ensure_containers_attribute_is_a_flat_array
  after_commit :queue_build_assesments_job, on: :create

  def self.most_recent
    order(:created_at).last
  end

  def increment_assessments_in_flight
    increment! :assessments_in_flight
  end

  def decrement_assessments_in_flight
    decrement! :assessments_in_flight
    # <=0 is used to handle the case where a ScheduleContainerJob starts and fails before assessment.start! is called
    post_process_completed_scan if assessments_in_flight <= 0
    assessments_in_flight
  end

  def machine_count
    organization&.machines&.count || 1
  end

  # always get an organization
  def resolved_organization
    organization || machine.organization
  end

  private

  # Callback Definitions
  def ensure_containers_attribute_is_a_flat_array
    return containers.flatten! if containers.is_a?(Array)
    self.containers = []
  end

  def queue_build_assesments_job
    AssessmentBuilderJob.perform_later self
  end

  # Validation Definitions
  def check_if_scan_complete
    return true if finished_at.present?
    errors.add(:base, 'Scan must be complete before removing')
    throw :abort
  end

  def containers_is_an_array_with_real_container_names
    containers_is_an_array? && containers_list_not_empty? && containers_list_only_contains_strings? && containers_exist?
  end

  def containers_is_an_array?
    unless containers.is_a? Array
      errors.add :containers, 'must be an array'
      return false
    end
    true
  end

  def containers_list_not_empty?
    if containers.empty?
      errors.add :containers, 'list must include at least one container name'
      return false
    end
    true
  end

  def containers_list_only_contains_strings?
    if containers.any? { |c| c.is_a?(String) == false }
      errors.add :containers, 'must only include strings'
      return false
    end
    true
  end

  def containers_exist?
    unless SecurityContainer.where(name: containers).count == containers.length
      errors.add :containers, 'one or more requested containers is invalid'
      return false
    end
    true
  end

  def post_process_completed_scan
    NotificationService.scan_complete(self)
    resolved_organization.auto_export_results(self)
  end
end
