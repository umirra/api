# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: iaas_configurations
#
#  id              :integer          not null, primary key
#  provider        :integer          not null
#  user_encrypted  :string
#  key_encrypted   :string
#  region          :string
#  project         :string
#  auth_url        :string
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_iaas_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_a8120b8d08  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class IaasConfiguration < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Secret Attributes
  include Vault::EncryptedModel
  vault_attribute :key
  vault_attribute :user, encrypted_column: :user_encrypted

  # Attribute Information
  enum provider: { openstack: 0, aws: 1 }

  # Validations
  validates :organization, presence: true
  validates :provider, presence: true
  validates :user, presence: true
  validates :key, presence: true
  with_options if: 'openstack?' do |c|
    c.validates :auth_url, presence: true
    c.validates :auth_url, format: { with: %r{\Ahttps?://\S+?/tokens/?\z}, message: 'must end with /tokens' }
    c.validates :project, presence: true
  end
  with_options if: 'aws?' do |c|
    c.validates :region, presence: true
    c.validates :auth_url, absence: true
    c.validates :project, absence: true
  end

  # Associations
  belongs_to :organization, inverse_of: :iaas_configuration
  has_many :iaas_discoveries, inverse_of: :iaas_configuration
end
