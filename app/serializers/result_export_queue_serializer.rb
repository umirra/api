# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id  (organization_id)
#  index_result_export_queues_on_type             (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class ResultExportQueueSerializer < ActiveModel::Serializer
  attributes :id, :organization_id, :type, :auto_sync, :created_by, :created_at, :updated_at
end
