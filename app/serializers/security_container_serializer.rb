# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_containers
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  category               :integer          default("whitebox"), not null
#  prog_args              :string           not null
#  default_config         :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  multi_host             :boolean          default(FALSE), not null
#  test_types             :string           default([]), not null, is an Array
#  configurable           :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  help_url               :string
#
# Indexes
#
#  index_security_containers_on_common_service_type_id  (common_service_type_id)
#  index_security_containers_on_name                    (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (common_service_type_id => common_service_types.id)
#
# rubocop:enable Metrics/LineLength

class SecurityContainerSerializer < ActiveModel::Serializer
  attributes :id, :name, :prog_args, :default_config, :category, :configurable, :help_url, :test_types
  belongs_to :common_service_type
end
