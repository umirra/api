# frozen_string_literal: true

class ResultExporter
  include Authority::Abilities

  attr_reader :export_queue, :results

  def initialize(export_queue, result_ids)
    @export_queue = export_queue
    @results = Result.where(id: result_ids).includes(assessment: { machine: :organization })
  end

  def publish
    # Publish to the default exchange
    Publisher.publish('', results_data, export_queue.amqp_queue_name)
  end

  def results_data
    {
      results: results.map { |result| ResultExporterSerializers::Result.new(result) },
      export_target: ResultExporterSerializers::ExportTarget.new(export_queue)
    }
  end
end

module ResultExporterSerializers
  class Result < ActiveModel::Serializer
    attributes :data, :meta

    def data
      ::ResultSerializer.new(object)
    end

    def meta
      {
        assessment: object.assessment.identifier,
        organization: Organization.new(object.assessment.machine.organization),
        machine: Machine.new(object.assessment.machine),
        scan_id: object.assessment.docker_command_id
      }
    end
  end

  class Machine < ActiveModel::Serializer
    attributes :id, :ip, :fqdn, :description, :name
  end

  class Organization < ActiveModel::Serializer
    attributes :id, :uid
  end

  class ExportTarget < ActiveModel::Serializer
    attributes :target_connection_settings
  end
end
