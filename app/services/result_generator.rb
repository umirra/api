# frozen_string_literal: true

class ResultGenerator
  def initialize(assessment)
    @assessment = assessment
  end

  def create_errored_result!
    create_result!(result_error_attrs)
  end

  def create_timed_out_result!
    create_result!(result_timeout_attrs)
  end

  private

  def create_result!(attrs)
    ActiveRecord::Base.transaction do
      @assessment.results.create! attrs
      @assessment.complete!
    end
  end

  def result_timeout_attrs
    attrs_to_hash = { nid: 'norad:timeout' }.merge common_attrs
    attrs_to_hash.merge(
      title: Result::ASSESSMENT_TIMEOUT_TITLE,
      output: Result::ASSESSMENT_TIMEOUT_OUTPUT,
      signature: OpenSSL::Digest::SHA256.hexdigest(attrs_to_hash.to_json)
    )
  end

  def result_error_attrs
    attrs_to_hash = { nid: 'norad:error' }.merge common_attrs
    attrs_to_hash.merge(
      title: Result::API_ERROR_TITLE,
      output: Result::API_ERROR_OUTPUT,
      signature: OpenSSL::Digest::SHA256.hexdigest(attrs_to_hash.to_json)
    )
  end

  def common_attrs
    { sir: 'no_impact', status: :error }
  end
end
