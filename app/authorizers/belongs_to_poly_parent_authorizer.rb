# frozen_string_literal: true

# This authorizer wraps up common access controls for RESTful Org / Machine STI-enabled children
class BelongsToPolyParentAuthorizer < Authority::Authorizer
  class << self
    # index
    def readable_by?(user, options)
      organization = options.fetch(:in).rbac_parent
      admin?(user, organization) || reader?(user, organization)
    end

    # create
    def creatable_by?(user, options)
      organization = options.fetch(:in).rbac_parent
      admin?(user, organization)
    end

    private

    def reader?(user, organization)
      user.has_role? :organization_reader, organization
    end

    def admin?(user, organization)
      user.has_role? :organization_admin, organization
    end
  end

  # show
  def readable_by?(user)
    admin?(user) || reader?(user)
  end

  # update
  def updatable_by?(user)
    admin?(user)
  end

  # destroy
  def deletable_by?(user)
    admin?(user)
  end

  private

  def admin?(user)
    user.has_role? :organization_admin, resource.send(poly_assoc_name).rbac_parent
  end

  def reader?(user)
    user.has_role? :organization_reader, resource.send(poly_assoc_name).rbac_parent
  end
end
