# frozen_string_literal: true

class OrganizationTokenAuthorizer < Authority::Authorizer
  def readable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end

  def updatable_by?(user)
    user.has_role? :organization_admin, resource.organization
  end
end
