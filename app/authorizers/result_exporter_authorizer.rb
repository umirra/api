# frozen_string_literal: true

class ResultExporterAuthorizer < Authority::Authorizer
  def self.creatable_by?(user, options)
    admin?(user, options) || reader?(user, options)
  end

  def self.reader?(user, options)
    user.has_role? :organization_reader, options[:in]
  end

  def self.admin?(user, options)
    user.has_role? :organization_admin, options[:in]
  end
end
