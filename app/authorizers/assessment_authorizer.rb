# frozen_string_literal: true

class AssessmentAuthorizer < Authority::Authorizer
  def self.reader?(user, options)
    user.has_role? :organization_reader, options[:in]
  end

  def self.admin?(user, options)
    user.has_role? :organization_admin, options[:in]
  end

  def self.readable_by?(user, options)
    reader?(user, options) || admin?(user, options)
  end

  def reader?(user)
    user.has_role? :organization_reader, resource.machine.organization
  end

  def admin?(user)
    user.has_role? :organization_admin, resource.machine.organization
  end

  def readable_by?(user)
    reader?(user) || admin?(user)
  end
end
