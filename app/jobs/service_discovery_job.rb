# frozen_string_literal: true

require './lib/norad_exception_notifier'

class ServiceDiscoveryJob < ActiveJob::Base
  include NoradExceptionNotifier

  queue_as :swarm_scheduling

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)
    Resque.logger.error "Exception while discovering assets: #{exception.inspect}" if defined?(Resque)
    # XXX: The first argument passed to the perform method is the discovery object.
    discovery = @arguments.first
    ActiveRecord::Base.transaction do
      discovery.error_message = 'Failed to schedule service discovery'
      discovery.fail!
    end
  end

  after_perform do |job|
    discovery = job.arguments.first
    discovery.start!
  end

  def perform(discovery)
    org = discovery.organization
    relay_queue = org.docker_relay_queue
    if relay_queue
      relay_opts = {
        relay_secret: org.relay_encryption_key,
        relay_queue: relay_queue,
        relay_exchange: org.token
      }
    end
    DockerSwarm.queue_discovery_container(discovery, relay_opts || {})
  end
end
