# frozen_string_literal: true

require './lib/norad_exception_notifier'

class ResultExportJob < ActiveJob::Base
  include NoradExceptionNotifier

  queue_as :result_export

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)
    queue = @arguments[0]
    Resque.logger.error "Exception while starting job: #{exception.inspect}" if defined?(Resque)
    Resque.logger.error "Unable to export results to queue: #{queue}"
  end

  def perform(queue, result_ids)
    ResultExporter.new(queue, result_ids).publish
  end
end
