class EnableVaultForSecurityContainerConfig < ActiveRecord::Migration[4.2]
  def up
    add_column :security_container_configs, :values_encrypted, :string
    SecurityContainerConfig.reset_column_information

    SecurityContainerConfig.all.each do |config|
      config.values = config.attributes['values']
      config.save!
    end

    remove_column :security_container_configs, :values
  end

  def down
    remove_column :security_container_configs, :values_encrypted
    add_column :security_container_configs, :values, :json
  end
end
