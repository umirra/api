class CreateServiceDiscoveries < ActiveRecord::Migration[4.2]
  def change
    create_table :service_discoveries do |t|
      t.references :machine, index: true
      t.references :container_secret, index: true
      t.integer :state
      t.string :error_message

      t.timestamps null: false
    end
    add_index :service_discoveries, [:machine_id, :state], where: "state = #{IaasDiscovery::IN_PROGRESS_STATE}", unique: true, name: 'index_service_discoveries_on_in_progress_state'
    add_index :service_discoveries, [:machine_id, :state], where: "state = #{IaasDiscovery::PENDING_STATE}", unique: true, name: 'index_service_discoveries_on_pending_state'
    add_foreign_key :service_discoveries, :machines, on_delete: :cascade
    add_foreign_key :service_discoveries, :security_container_secrets, column: :container_secret_id, on_delete: :cascade
  end
end
