class AddSecurityContainerForeignKeyToWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  def change
    add_reference :white_box_assessments, :security_container, index: true, foreign_key: true
  end
end
