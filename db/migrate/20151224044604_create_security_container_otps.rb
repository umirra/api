class CreateSecurityContainerOtps < ActiveRecord::Migration[4.2]
  def change
    create_table :security_container_otps do |t|
      t.string :secret
      t.references :assessment, polymorphic: true

      t.timestamps null: false
    end
    add_index :security_container_otps, :secret, unique: true
    add_index(:security_container_otps,
              [:assessment_type, :assessment_id],
              name: 'index_security_container_otps_on_assessment_type_and_id'
             )
  end
end
