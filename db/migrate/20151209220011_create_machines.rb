class CreateMachines < ActiveRecord::Migration[4.2]
  def change
    create_table :machines do |t|
      t.references :organization, index: true
      t.string :ip
      t.string :fqdn
      t.text :description
      t.integer :machine_status, default: 0, null: false

      t.timestamps null: false
    end
    add_foreign_key :machines, :organizations, on_delete: :cascade
  end
end
