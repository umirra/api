# This is gross
class FixupSecurityContainerArgsForNonQualys < ActiveRecord::Migration[4.2]
  SSL_CONTS = %w(nmap-ssl-dh-param sslyze-heartbleed nmap-psb-pubcryp-2 psb-x509-cert-validation psb-tls-curr-3).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end
  WHOLE = %w(qualys nmap-list-udp-ports nmap-list-tcp-ports).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end
  BRUTE = %w(hydra-postgres hydra-ftp hydra-mysql hydra-psb).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end
  AUTH = %w(glance neutron keystone cinder dashboard nova cis-benchmark-redhat psb-informative psb-evaluative docker-host-cis-benchmarks-info docker-host-cis-benchmarks qualys-authenticated vuls nginx-hardening).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end
  SSH_CRYP = %w(nmap-psb-sec-cry-prim).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end
  WEB_APP = %w(arachni-xss arachni-sql qualys-was zap-passive).map do |n|
    "norad-registry.cisco.com:5000/#{n}:0.0.1"
  end

  def up
    # ones to just drop default args
    # and nil out values in configs
    %w(
      nmap-ssl-dh-param
      sslyze-heartbleed
      hydra-postgres
      hydra-ftp
      hydra-mysql
      nmap-psb-sec-cry-prim
      nmap-psb-pubcryp-2
      psb-x509-cert-validation
      vuls
      zap-passive
      psb-tls-curr-3
    ).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{test}:0.0.1")
      next unless container
      container.default_config = {}
      container.test_types = determine_test_types(container.name)
      container.save!
      container.security_container_configs.each do |config|
        config.values = {}
        config.save!
      end
    end

    # tests that need prog args updated
    # and nil out values in configs
    %w(
      glance
      neutron
      keystone
      cinder
      dashboard
      nova
      cis-benchmark-redhat
      docker-host-cis-benchmarks-info
      docker-host-cis-benchmarks
      nginx-hardening
    ).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{test}:0.0.1")
      next unless container
      new_args = container.prog_args.gsub(/%{test_regex}/, container.default_config['test_regex'])
      container.default_config = {}
      container.test_types = determine_test_types(container.name)
      container.prog_args = new_args
      container.save!
      container.security_container_configs.each do |config|
        config.values = {}
        config.save!
      end
    end

    %w(xss sql).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/arachni-#{test}:0.0.1")
      next unless container
      new_args = container.prog_args.gsub(/%{check}/, container.default_config['check'])
      container.default_config = {}
      container.prog_args = new_args
      container.test_types = determine_test_types(container.name)
      container.save!
      container.security_container_configs.each do |config|
        config.values = {}
        config.save!
      end
    end

    %w(tcp udp).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/nmap-list-#{test}-ports:0.0.1")
      next unless container
      new_args = container.prog_args.gsub(/%{type}/, container.default_config['type'])
      new_args.gsub!(/%{port}/, '1-65535')
      container.default_config = {}
      container.prog_args = new_args
      container.test_types = determine_test_types(container.name)
      container.save!
      container.security_container_configs.each do |config|
        config.values = {}
        config.save!
      end
    end

    # web service updates
    %w(arachni-xss arachni-sql zap-passive).each do |test|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{test}:0.0.1")
      next unless container
      new_args = container.prog_args.gsub(/protocol/, 'web_service_protocol')
      new_args.gsub!(/url/, 'web_service_starting_page_path')
      container.prog_args = new_args
      container.test_types = determine_test_types(container.name)
      container.save!
    end

    # we won't fix up any qualys configs, but let's go ahead and update the containers
    qualys = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys:0.0.1')
    if qualys
      qualys.test_types = determine_test_types(qualys.name)
      qualys.save!
    end

    qualys_was = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1')
    if qualys_was
      qualys_was.test_types = determine_test_types(qualys_was.name)
      qualys_was.save!
    end

    qualys_auth = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys-authenticated:0.0.1')
    if qualys_auth
      qualys_auth.test_types = determine_test_types(qualys_auth.name)
      qualys_auth.save!
    end

  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end

  def determine_test_types(container_name)
    if SSL_CONTS.include?(container_name)
      ['ssl_crypto']
    elsif WHOLE.include?(container_name)
      ['whole_host']
    elsif BRUTE.include?(container_name)
      ['brute_force']
    elsif AUTH.include?(container_name)
      ['authenticated']
    elsif SSH_CRYP.include?(container_name)
      ['ssh_crypto']
    elsif WEB_APP.include?(container_name)
      ['web_application']
    end
  end
end
