# This is a long, ugly script. Hold your nose
class CreateServicesFromTestConfigs < ActiveRecord::Migration[4.2]
  def add_web_services_for_zap(config, machine)
    if config.values['protocol'] == 'http'
      machine.web_application_services.where(port: 80).first_or_create! do |s|
        s.name = 'web-service-80'
      end
    elsif config.values['protocol'] == 'https'
      machine.web_application_services.where(port: 443).first_or_create! do |s|
        s.name = 'web-service-443'
        s.encryption_type = 'ssl'
      end
    end
  end

  def add_web_services_for_arachni(config, machine)
    port = config.values['port']
    return if port.to_s =~ /\A%{/
    webapp = machine.web_application_services.where(port: port).first_or_create! do |s|
      s.name = "web-service-#{config.values['port']}"
      s.encryption_type = config.values['protocol'] == 'https' ? 'ssl' : 'cleartext'
    end
    if webapp.web_application_config
      webapp.web_application_config.starting_page_path = config.values['url']
      webapp.web_application_config.save!
    else
      WebApplicationConfig.create!(web_application_service: webapp, starting_page_path: config.values['url'])
    end
  end

  def add_web_services_for_qualys_was(config, machine)
    # Maybe they specified port in the URL
    if config.values['target_url'] =~ /:\d+/
      port = config.values['target_url'].scan(/:(\d+)/).first
    elsif config.values['target_url'] =~ /\Ahttps:/
      port =  443
    else
      port = 80
    end
    webapp = machine.web_application_services.where(port: port).first_or_create! do |s|
      s.encryption_type = port == 443 ? 'ssl' : 'cleartext'
      s.name = "web-service-#{port}"
    end

    wconfig = WebApplicationConfig.where(web_application_service: webapp).first_or_initialize
    wconfig.starting_page_path ||= '/'
    wconfig.url_blacklist ||= config.values['coma_sep_blacklist_urls']
    wconfig.login_form_username_field_name ||= 'username'
    wconfig.login_form_password_field_name ||= 'password'
    wconfig.auth_type ||= 'form'
    wconfig.save!

    identity = ServiceIdentity.where(service: webapp).first_or_initialize
    identity.username = config.values['web_username']
    identity.password = config.values['web_password']
    identity.save!
  end

  def add_ssh_service(config, machine)
    port = config.values['port'] || 22
    return if port.to_s =~ /\A%{/
    machine.ssh_services.where(port: port).first_or_create! do |s|
      s.name = "ssh-service-#{port}"
    end
  end

  def add_generic_service(config, machine, crypto = 'plaintext')
    port = config.values['port']
    return if port.to_s =~ /\A%{/
    s = machine.services.find_by(port: port)
    if s
      s.encryption_type = crypto
      s.save!
    else
      machine.services << GenericService.new(port: port, encryption_type: crypto, name: "generic-service-#{port}")
    end
  end

  def toggle_brute_force(config, machine)
    s = machine.services.find_by(port: config.values['port'])
    if s
      s.allow_brute_force = true
      s.save!
    else
      machine.services << GenericService.new(port: config.values['port'], name: "generic-service-#{config.values['port']}", allow_brute_force: true)
    end
  end

  def up
    # Start with specifics...
    zap = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/zap-passive:0.0.1')
    arachni1 = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/arachni-xss:0.0.1')
    arachni2 = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/arachni-sql:0.0.1')
    qualys_was = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1')
    # For machines...
    SecurityContainerConfig.where(security_container: zap, organization_id: nil).each do |config|
      add_web_services_for_zap(config, config.machine)
    end

    SecurityContainerConfig.where(security_container: arachni1, organization_id: nil).each do |config|
      add_web_services_for_arachni(config, config.machine)
    end

    SecurityContainerConfig.where(security_container: arachni2, organization_id: nil).each do |config|
      add_web_services_for_arachni(config, config.machine)
    end

    SecurityContainerConfig.where(security_container: qualys_was, organization_id: nil).each do |config|
      add_web_services_for_qualys_was(config, config.machine)
    end
    # For orgs...
    SecurityContainerConfig.where(security_container: zap, machine_id: nil).each do |config|
      config.organization.machines.each do |machine|
        add_web_services_for_zap(config, machine)
      end
    end

    SecurityContainerConfig.where(security_container: arachni1, machine_id: nil).each do |config|
      config.organization.machines.each do |machine|
        add_web_services_for_arachni(config, machine)
      end
    end

    SecurityContainerConfig.where(security_container: arachni2, machine_id: nil).each do |config|
      config.organization.machines.each do |machine|
        add_web_services_for_arachni(config, machine)
      end
    end

    SecurityContainerConfig.where(security_container: qualys_was, machine_id: nil).each do |config|
      config.organization.machines.each do |machine|
        add_web_services_for_qualys_was(config, machine)
      end
    end

    # Then do authenticated
    %w(serverspec glance neutron keystone cinder dashboard nova cis-benchmark-redhat psb-informative psb-evaluative docker-host-cis-benchmarks vuls nginx-hardening qualys-authenticated).each do |t|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{t}:0.0.1")
      SecurityContainerConfig.where(security_container: container, organization_id: nil).each do |config|
        add_ssh_service(config, config.machine)
      end

      SecurityContainerConfig.where(security_container: container, machine_id: nil).each do |config|
        config.organization.machines.each do |machine|
          add_ssh_service(config, machine)
        end
      end
    end

    # Then generic ssl crypto
    %w(nmap-ssl-dh-param sslyze-heartbleed nmap-psb-pubcryp-2 psb-x509-cert-validation psb-tls-curr3).each do |t|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{t}:0.0.1")
      SecurityContainerConfig.where(security_container: container, organization_id: nil).each do |config|
        add_generic_service(config, config.machine, 'ssl')
      end

      SecurityContainerConfig.where(security_container: container, machine_id: nil).each do |config|
        config.organization.machines.each do |machine|
          add_generic_service(config, machine, 'ssl')
        end
      end
    end

    # Then generic ssh crypto
    container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/nmap-psb-sec-cry-prim:0.0.1")
    SecurityContainerConfig.where(security_container: container, organization_id: nil).each do |config|
      add_generic_service(config, config.machine, 'ssh')
    end

    SecurityContainerConfig.where(security_container: container, machine_id: nil).each do |config|
      config.organization.machines.each do |machine|
        add_generic_service(config, machine, 'ssh')
      end
    end

    # Now toggle brute force
    %w(postgres ftp mysql).each do |t|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/hydra-#{t}:0.0.1")
      SecurityContainerConfig.where(security_container: container, organization_id: nil).each do |config|
        toggle_brute_force(config, config.machine)
      end

      SecurityContainerConfig.where(security_container: container, machine_id: nil).each do |config|
        config.organization.machines.each do |machine|
          toggle_brute_force(config, machine)
        end
      end
    end
  end

  def down
    ActiveRecord::IrreversibleMigration
  end
end
