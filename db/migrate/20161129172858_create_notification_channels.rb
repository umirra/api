class CreateNotificationChannels < ActiveRecord::Migration[5.0]
  def change
    create_table :notification_channels do |t|
      t.boolean :enabled, default: true
      t.string :event
      t.integer :organization_id

      t.timestamps
    end
    add_index :notification_channels, :event
    add_index :notification_channels, [:organization_id, :event], unique: true
  end
end
