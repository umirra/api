class MarkQualysContainersAsConfigurable < ActiveRecord::Migration[4.2]
  def up
    %w(qualys qualys-authenticated).each do |q|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{q}:0.0.1")
      next unless container
      container.configurable = true
      container.save!
    end
  end

  def down
    %w(qualys qualys-authenticated).each do |q|
      container = SecurityContainer.find_by(name: "norad-registry.cisco.com:5000/#{q}:0.0.1")
      next unless container
      container.configurable = false
      container.save!
    end
  end
end
