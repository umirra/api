class CreateSecurityContainerConfigs < ActiveRecord::Migration[4.2]
  def change
    create_table :security_container_configs do |t|
      t.json :values
      t.references :configurable, polymorphic: true
      t.references :security_container, foreign_key: true
    end
    add_index(:security_container_configs,
              [:configurable_type, :configurable_id],
              name: 'index_security_container_configs_on_configurable_type_and_id'
             )
    add_index(:security_container_configs,
              :security_container_id,
              name: 'index_security_container_configs_on_security_container_id'
             )
  end
end
