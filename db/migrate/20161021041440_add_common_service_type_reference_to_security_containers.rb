class AddCommonServiceTypeReferenceToSecurityContainers < ActiveRecord::Migration[4.2]
  def up
    add_reference :security_containers, :common_service_type, index: true, foreign_key: true
    container = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/hydra-postgres:0.0.1')
    container&.common_service_type = CommonServiceType.find_by(name: 'postgresql')
    container&.save!
    container = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/hydra-mysql:0.0.1')
    container&.common_service_type = CommonServiceType.find_by(name: 'mysql')
    container&.save!
    container = SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/hydra-ftp:0.0.1')
    container&.common_service_type = CommonServiceType.find_by(name: 'ftp')
    container&.save!
  end

  def down
    remove_reference :security_containers, :common_service_type
  end
end
