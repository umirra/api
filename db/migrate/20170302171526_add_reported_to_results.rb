class AddReportedToResults < ActiveRecord::Migration[5.0]
  def change
    add_column :results, :reported, :boolean, default: false, null: false
  end
end
