class UpdateProgArgsNDcFieldsForSecurityContainer < ActiveRecord::Migration[4.2]
  class SecurityContainer < ActiveRecord::Base
  end

  def up
    say_with_time 'Migrating Qualys-WAS Config to support form-based login' do
      sc = SecurityContainer.find_by name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1'
      return unless sc
      say_with_time('Found the container.sc Updating the default_config and prog_args values') do
        old_vals = sc.default_config
        sc.default_config = nil # to let the migration know that this field changed
        old_vals['qualys_scanner'] = 'SJC23-IPv6-2'
        sc.default_config = old_vals
        sc.prog_args = '-r %{web_service_protocol}://%{target}:%{port}%{web_service_starting_page_path} '\
                       '-u %{qualys_username} '\
                       '-p %{qualys_password} '\
                       '--web_service_auth_type %{web_service_auth_type} '\
                       '--web_service_login_form_username_field_name %{web_service_login_form_username_field_name} '\
                       '-w %{service_username} '\
                       '--web_service_login_form_password_field_name %{web_service_login_form_password_field_name} '\
                       '-v %{service_password} '\
                       '-t %{option_profile} '\
                       '-q %{qualys_scanner} '\
                       '-b %{web_service_url_blacklist}'
        sc.save!
      end
    end
  end

  def down
    say_with_time 'Rollback Qualys-WAS Config to support form-based login' do
      sc = SecurityContainer.find_by name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1'
      return unless sc
      say_with_time('Found the container.sc Updating and prog_args value with old settings') do
        # In sc.default_config don't change the scanner back to RTP-3. Leave it with updated value.
        sc.prog_args = '-h %{target} '\
                       '-r %{web_service_protocol}://%{target}:%{port}%{web_service_starting_page_path} '\
                       '-u %{qualys_username} '\
                       '-p %{qualys_password} '\
                       '-w %{service_username} '\
                       '-v %{service_password} '\
                       '-t %{option_profile} '\
                       '-q %{qualys_scanner} '\
                       '-b %{web_service_url_blacklist}'
        sc.save!
      end
    end
  end
end
