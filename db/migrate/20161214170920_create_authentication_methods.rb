class CreateAuthenticationMethods < ActiveRecord::Migration[5.0]
  def change
    create_table :authentication_methods do |t|
      t.integer :user_id
      t.string :type

      t.timestamps
    end
    add_index :authentication_methods, :user_id, unique: true
    add_index :authentication_methods, :type

    reversible do |direction|
      direction.up { set_default_authentication_method_for_users }
    end
  end

  private

  def set_default_authentication_method_for_users
    execute <<~SQL
      INSERT INTO authentication_methods (user_id, type, created_at, updated_at)
      SELECT id, 'ReverseProxyAuthenticationMethod', now(), now()
      FROM users;
    SQL
  end
end
