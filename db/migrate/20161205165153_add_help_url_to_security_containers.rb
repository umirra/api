class AddHelpUrlToSecurityContainers < ActiveRecord::Migration[5.0]
  def change
    add_column :security_containers, :help_url, :string
  end
end
