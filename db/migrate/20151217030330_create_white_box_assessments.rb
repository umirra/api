class CreateWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  def change
    create_table :white_box_assessments do |t|
      t.string :identifier, null: false
      t.references :machine, index: true
      t.references :command, polymorphic: true, index: true
      t.integer :state, default: 0, null: false
      t.integer :category, default: 0, null: false
      t.string :title
      t.string :description

      t.timestamps null: false
    end
    add_index :white_box_assessments, :identifier, unique: true
    add_foreign_key :white_box_assessments, :machines, on_delete: :cascade
  end
end
