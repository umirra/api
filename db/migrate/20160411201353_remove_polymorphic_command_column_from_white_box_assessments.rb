class RemovePolymorphicCommandColumnFromWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  class WhiteBoxAssessment < ActiveRecord::Base
    belongs_to :command, polymorphic: true
    belongs_to :agent_command
    belongs_to :docker_command
  end

  def up
    change_table :white_box_assessments do |t|
      t.references :agent_command, index: true
      t.references :docker_command, index: true
    end
    add_foreign_key :white_box_assessments, :agent_commands, on_delete: :cascade
    add_foreign_key :white_box_assessments, :docker_commands, on_delete: :cascade
    WhiteBoxAssessment.reset_column_information

    WhiteBoxAssessment.all.each do |assessment|
      if assessment.command_type == 'AgentCommand'
        assessment.agent_command_id = assessment.command_id
      else
        assessment.docker_command_id = assessment.command_id
      end
      assessment.save!
    end

    change_table :white_box_assessments do |t|
      t.remove :command_type
      t.remove :command_id
    end
  end

  def down
    change_table :white_box_assessments do |t|
      t.references :command, polymorphic: true, index: true
    end
    WhiteBoxAssessment.reset_column_information

    WhiteBoxAssessment.all.each do |assessment|
      if assessment.agent_command
        assessment.command = assessment.agent_command
      elsif assessment.docker_command
        assessment.command = assessment.docker_command
      end
      assessment.save!
    end

    change_table :white_box_assessments do |t|
      t.remove :agent_command_id
      t.remove :docker_command_id
    end
  end
end
