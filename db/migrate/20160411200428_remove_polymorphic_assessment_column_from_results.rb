class RemovePolymorphicAssessmentColumnFromResults < ActiveRecord::Migration[4.2]
  class Result < ActiveRecord::Base
    belongs_to :assessment, polymorphic: true
    belongs_to :white_box_assessment
    belongs_to :black_box_assessment
  end

  def up
    change_table :results do |t|
      t.references :white_box_assessment, index: true
      t.references :black_box_assessment, index: true
    end
    add_foreign_key :results, :white_box_assessments, on_delete: :cascade
    add_foreign_key :results, :black_box_assessments, on_delete: :cascade
    Result.reset_column_information

    Result.all.each do |result|
      if result.assessment_type == 'WhiteBoxAssessment'
        result.white_box_assessment_id = result.assessment_id
      else
        result.black_box_assessment_id = result.assessment_id
      end
      result.save!
    end

    change_table :results do |t|
      t.remove :assessment_type
      t.remove :assessment_id
    end
  end

  def down
    change_table :results do |t|
      t.references :assessment, polymorphic: true, index: true
    end
    Result.reset_column_information

    Result.all.each do |result|
      if result.white_box_assessment
        result.assessment = result.white_box_assessment
      elsif result.black_box_assessment
        result.assessment = result.black_box_assessment
      end
      result.save!
    end

    change_table :results do |t|
      t.remove :white_box_assessment_id
      t.remove :black_box_assessment_id
    end
  end
end
