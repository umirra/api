class CreateDockerCommands < ActiveRecord::Migration[4.2]
  def change
    create_table :docker_commands do |t|
      t.references :commandable, polymorphic: true, index: true
      t.text :error_details
      t.json :containers

      t.timestamps null: false
    end
  end
end
