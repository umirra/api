class AddKeySignatureToMachineAgents < ActiveRecord::Migration[4.2]
  class MachineAgent < ActiveRecord::Base
  end

  def up
    add_column :machine_agents, :key_signature, :string
    MachineAgent.all.each do |agent|
      agent.send(:generate_key_signature)
      agent.save!
    end
  end

  def down
    remove_column :machine_agents, :key_signature
  end
end
