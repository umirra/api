class CreateServiceIdentities < ActiveRecord::Migration[4.2]
  def change
    create_table :service_identities do |t|
      t.string :username_encrypted
      t.string :password_encrypted
      t.references :service, index: true
      t.timestamps null: false
    end
    add_foreign_key :service_identities, :services, on_delete: :cascade
  end
end
