class UpdateExistingAuthenticatedTestConfigsDataOnly < ActiveRecord::Migration[5.0]
  def up
    tests.each do |test|
      sc = SecurityContainer.find_by(name: test)
      next unless sc
      sc.prog_args = '--detect-os -h %{target} -u %{ssh_user} -t tests --cisco-enable-pw=%{cisco_enable_pw} -p %{port} %{ssh_key}'
      sc.default_config = { cisco_enable_pw: '' }
      sc.save!
      sc.security_container_configs.each do |config|
        config.values = sc.default_config
        config.save!
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end

  private

  def tests
    %w[sec-aut-accdef sec-cre-nolock sec-ops-strength sec-cre-limtry-2 sec-cry-stdcode].map do |test|
      "norad-registry.cisco.com:5000/#{test}"
    end
  end
end
