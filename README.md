[![build status](https://gitlab.com/norad/api/badges/master/build.svg)](https://gitlab.com/norad/api/commits/master)
[![coverage report](https://gitlab.com/norad/api/badges/master/coverage.svg)](https://gitlab.com/norad/api/commits/master)

# Norad API

The Norad API is the central component of the Norad Security Assessment as a
Service Offering. It maintains assets, results, test configuration, and test
results. It is also responsible for scheduling security tests.

## Getting Started

The easiest way to get up and running is to provision a
[Norad Dev Box](https://gitlab.com/norad/dev-box).

## Loading Remote Security Tests

The utility script found at ```utilities/load_security_containers_from_repo.rb``` can be used to
pull in remote Security Test data. It currently supports loading in an artifact from a Gitlab CI
build.

```sh
$> bin/rails runner utilities/load_security_containers_from_repo.rb <url_to_artifact>
```

## Running Security Tests

The API uses Resque workers for scheduling security tests with a Docker
instance. The simplest way to get your resque workers up and running is to start
them with the following command:

```sh
$> VERBOSE=1 QUEUE=* rake environment resque:work
```

## Testing Airbrake

Airbrake can be tested from the dev-box, but it must be tested in the
production environment. To test Airbrake from the dev-box, do the following:
1. Create a .env.production file if it does not exist.
2. Add the following environment variables to .env.production.

WARNING: Do not use the api key for the production Norad API or Norad UI.
For testing, please use the Norad DevBox key so that everyone does not receive
Airbrake notifications that appear to be production problems. Email
notifications are not sent for the Norad DevBox, but they will be for the
Norad API and Norad UI.

ERRBIT_URL=http://localhost:8080
AIRBRAKE_API_KEY=<The correct Norad DevBox project key.>
REDIS_CACHE_URL=http://localhost
REDIS_CACHE_PORT=6379
REDIS_BG_JOBS_URL=http://localhost
REDIS_BG_JOBS_PORT=6379

3. Comment out the contents of the following initializer:

config/initializers/dkim_configuration.rb

4. When sshing to the dev-box from the host, use the following command:

vagrant ssh -- -R 8080:localhost:8080

5. On the machine hosting the dev-box, run the following command:

ssh -N -L 8080:192.168.2.36:8080 -o UserKnownHostsFile=~/.ssh/known_hosts.norad.prod -o IdentityFile=~/.ssh/id_rsa -o HostKeyAlias=jumphost.norad.prod errbit-user@128.107.32.180 

6. Run the following command form the api root directory in the dev-box:

RAILS_ENV=production bundle exec rake airbrake:test

To watch the message on the dev-box or host, use the following command:

sudo tcpdump -As0 -ni any port 8080

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of
conduct and the process for submitting merge requests.
