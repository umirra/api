# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = false

  # Use a different cache store in production.
  config.cache_store = :readthis_store, {
    namespace: 'cache',
    redis: { host: ENV.fetch('REDIS_CACHE_URL'), port: ENV.fetch('REDIS_CACHE_PORT'), driver: :hiredis }
  }

  # Use a real queuing backend for Active Job (and separate queues per environment)
  config.active_job.queue_adapter = :resque
  # config.active_job.queue_name_prefix = "norad_api_#{Rails.env}"
  config.action_mailer.perform_caching = false

  # ActionMailer Configuration
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: ENV.fetch('SMTP_SERVER'),
    port: ENV.fetch('SMTP_PORT', 25)
  }
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true
  config.action_mailer.default_url_options = { protocol: 'https', host: 'wwwin-norad.cisco.com' }

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use lograge
  config.lograge.enabled = true

  config.log_level = :info
  if defined?(Rails::Console)
    # Be sure not to log anything if we are in a console
    config.logger = ActiveSupport::Logger.new('/dev/null')
    # Give me lots of AR output in the console
    config.after_initialize { ActiveRecord::Base.logger.level = 0 }
  else
    config.logger = ActiveSupport::Logger.new(STDOUT) if ENV['RAILS_LOG_TO_STDOUT'].present?
    config.active_record.logger = nil

    config.lograge.custom_options = lambda do |event|
      exceptions = %w[controller action format id]
      {
        request_id: event.payload[:request_id],
        user: event.payload[:user],
        remote_ip: event.payload[:remote_ip],
        app: 'api',
        params: event.payload[:params].except(*exceptions)
      }
    end
  end

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # Add list of trusted proxy servers
  config.action_dispatch.trusted_proxies = %w[10.0.2.2]
end
# rubocop:enable Metrics/BlockLength
