# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  if Rails.env.development?
    require 'resque/server'
    mount Resque::Server.new, at: '/resque'
  end
  namespace 'v1' do
    resources :users, only: %i[index show update create] do
      resource 'api_token', only: :update
      post 'authenticate', on: :member
      post 'authenticate', on: :collection
    end
    resources :organizations, except: %i[new edit] do
      resources :result_ignore_rules, only: %i[create index destroy]
      resources :docker_commands, only: %i[create index]
      resources :machines, only: %i[create index]
      resources :security_container_configs, only: %i[index create]
      resources :docker_relays, only: :index
      resource :scan, only: :create
      resources :memberships, only: %i[create index]
      resources :scan_schedules, only: %i[create index], controller: :organization_scan_schedules
      resource :organization_token, only: :update
      resources :requirements, only: :index, controller: :organization_requirements
      resources :enforcements, only: %i[index create]
      resource :iaas_configuration, only: %i[create show]
      resources :iaas_discoveries, only: %i[create index]
      resources :ssh_key_pairs, only: %i[create index]
      resources :notification_channels, only: :index
      resources :result_export_queues, only: :index
      resources :jira_export_queues, only: :create
      resources :infosec_export_queues, only: :create
    end
    resources :machines, only: %i[show update destroy create] do
      resources :result_ignore_rules, only: %i[create index destroy]
      resources :docker_commands, only: %i[create index]
      resources :assessments, only: :index do
        get 'latest', on: :collection
      end
      resources :security_container_configs, only: %i[index create]
      resources :services, only: %i[index create]
      resources :service_discoveries, only: %i[index create]
      resource :scan, only: :create
      resources :scan_schedules, only: %i[create index], controller: :machine_scan_schedules
      resource :ssh_key_pair_assignment, only: %i[create destroy]
    end
    resources :result_ignore_rules, only: %i[show update destroy]
    resources :docker_commands, only: %i[show destroy]
    resources :assessments, only: :show do
      resources :results, only: :create
    end
    resources :docker_relays, only: %i[create update destroy show] do
      member do
        post :heartbeat
      end
    end
    resources :security_container_configs, only: %i[show update destroy]
    resources :security_containers, only: %i[index show]
    resources :services, only: %i[show update destroy] do
      resources :web_application_configs, only: :create
      resources :service_identities, only: :create
    end
    resources :ssh_key_pairs, only: %i[show update destroy]
    resources :notification_channels, only: %i[show update]
    resources :jira_export_queues, only: %i[update show destroy]
    resources :infosec_export_queues, only: %i[update show destroy]
    resources :result_exporters, only: :create
    resources :organization_configurations, only: %i[show update]
    resources :memberships, only: [:destroy]
    resources :machine_scan_schedules, only: %i[update show destroy]
    resources :organization_scan_schedules, only: %i[update show destroy]
    resources :enforcements, only: :destroy
    resources :requirement_groups, only: %i[index show create update] do
      resources :requirements, only: :create
    end
    resources :requirements, only: %i[show update destroy] do
      resources :provisions, only: :create
    end
    resources :provisions, only: :destroy
    resources :iaas_configurations, only: %i[show update destroy]
    resources :iaas_discoveries, only: :show
    resources :service_discoveries, only: %i[show update]
    resources :service_identities, only: :update
    resources :web_application_configs, only: :update
    resources :password_resets, only: %i[create update show], param: :provided_token
    resources :email_confirmations, only: :create, param: :email_confirmation_token
  end
  mount(ClientTestRouteHelpers::Engine, at: '/client_test_route_helpers') if Rails.env.client_test?
end
# rubocop:enable Metrics/BlockLength
