# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Configure sensitive parameters which will be filtered from the log file.
Rails.application.config.filter_parameters += [
  :password,
  'results.nid',
  'results.output',
  'results.description',
  'results.title',
  'security_container_config.values',
  'ssh_key_pair.username',
  'ssh_key_pair.key',
  'service_identity.username',
  'service_identity.password',
  'iaas_configuration.user',
  'iaas_configuration.key'
]
