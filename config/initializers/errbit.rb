# frozen_string_literal: true

if Rails.env.production?
  # Add Errbit
  Airbrake.configure do |config|
    config.host = ENV.fetch('ERRBIT_URL')
    config.project_id = 1
    config.project_key = ENV.fetch('AIRBRAKE_API_KEY')
    # if we ever introduce a staging environment, this variable will be useful.
    config.environment = Rails.env
  end
  Airbrake.add_filter do |notice|
    blocked_notice = notice[:errors].any? do |error|
      error[:type] == 'ActiveRecord::RecordNotFound' ||
        error[:type] == 'ActionController::RoutingError'
    end
    notice.ignore! if blocked_notice
  end

  # Add Airbrake as a failure backend for Resque jobs. Note this does not work
  # for jobs that are plain Ruby classes as the airbrake-ruby gem does not wrap
  # exceptions. The airbrake gem, however, wraps for Rails classes. For more
  # exploration, see https://github.com/airbrake/airbrake/tree/master/lib/airbrake/rails
  require 'resque/failure/airbrake'
  require 'resque/failure/multiple'
  require 'resque/failure/redis'
  Resque::Failure::Multiple.classes = [Resque::Failure::Redis, Resque::Failure::Airbrake]
  Resque::Failure.backend = Resque::Failure::Multiple
end
