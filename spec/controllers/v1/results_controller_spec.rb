# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ResultsController, type: :controller do
  include NoradControllerTestHelpers
  before :each do
    @standard_user = create :user
  end

  # Purposefully create an invalid nid here because the controller is going to provide the namespace
  let(:valid_result) { attributes_for(:result, nid: "qid#{SecureRandom.hex(8)}") }

  describe 'POST #create for White Box Assessments' do
    # The API token check should be skipped for this action
    before :each do
      allow(controller).to receive(:require_api_token).and_call_original
    end

    def create_results(wba_id, payload, xheaders = {})
      request.headers.merge! xheaders
      request.headers['rack.request.form_hash'] = payload
      norad_post :create, { assessment_id: wba_id }.merge(payload)
    end

    context 'with valid params' do
      before :each do
        allow(controller).to receive(:require_signature).and_return(true)
      end

      after :each do
        allow(controller).to receive(:require_signature).and_call_original
      end

      it 'creates a result for a white box assessment' do
        n_results = 1
        wba = create :white_box_assessment
        expect(wba.state).to_not eq 'complete'
        old_count = wba.results.count
        create_results wba.to_param, results: [valid_result]
        expect(response.status).to be 200
        expect(wba.results.reload.count).to eq old_count + n_results
        expect(wba.reload.state).to eq 'complete'
      end

      it 'creates multiple results for a white box assessment' do
        n_results = 3
        wba = create :white_box_assessment
        old_count = wba.results.count
        create_results wba.to_param, results: Array.new(n_results) { valid_result }
        expect(response.status).to be 200
        expect(wba.results.reload.count).to eq old_count + n_results
      end

      it 'updates the state transition time of the white box assessment' do
        wba = create :white_box_assessment
        initial_time = wba.state_transition_time
        create_results wba.to_param, results: [valid_result]
        expect(wba.reload.state_transition_time).to_not eq(initial_time)
      end
    end

    context 'with invalid params' do
      before :each do
        allow(controller).to receive(:require_signature).and_return(true)
      end

      it 'responds with an error message' do
        wba = create :white_box_assessment
        expect do
          create_results wba.to_param, results: [attributes_for(:result, status: nil)]
        end.to change(Result, :count).by(1)
        error_message = 'Security test post values not valid: Validation failed: Status ' \
                        "can't be blank, Nid can only contain alphanumeric, dashes, and underscores"
        expect(Result.last.description).to eq error_message
        expect(response.status).to eq 200
      end

      it 'creates an error result when results are missing' do
        wba = create :white_box_assessment
        expect do
          create_results wba.to_param, results: []
        end.to raise_error ActionController::ParameterMissing
        error_message = 'Security test results are missing critical information:' \
                        ' param is missing or the value is empty: results'
        expect(Result.last.description).to eq error_message
      end
    end
  end

  describe 'POST #create for Black Box Assessments' do
    # The API token check should be skipped for this action
    before :each do
      allow(controller).to receive(:require_api_token).and_call_original
    end

    def create_results(bba_id, payload, xheaders = {})
      request.headers.merge! xheaders
      request.headers['rack.request.form_hash'] = payload
      norad_post :create, { assessment_id: bba_id }.merge(payload)
    end

    context 'with valid params' do
      before :each do
        allow(controller).to receive(:require_signature).and_return(true)
      end

      it 'creates a result for a black box assessment' do
        n_results = 1
        bba = create :black_box_assessment
        expect(bba.state).to_not eq 'complete'
        old_count = bba.results.count
        create_results bba.to_param, results: [valid_result]
        expect(response.status).to be 200
        expect(bba.results.reload.count).to eq old_count + n_results
        expect(bba.reload.state).to eq 'complete'
      end

      it 'creates multiple results for a black box assessment' do
        n_results = 3
        bba = create :black_box_assessment
        old_count = bba.results.count
        create_results bba.to_param, results: Array.new(n_results) { valid_result }
        expect(response.status).to be 200
        expect(bba.results.reload.count).to eq old_count + n_results
      end

      it 'updates the state transition time of the black box assessment' do
        bba = create :black_box_assessment
        initial_time = bba.state_transition_time
        create_results bba.to_param, results: [valid_result]
        expect(bba.reload.state_transition_time).to_not eq(initial_time)
      end
    end

    context 'with invalid params' do
      before :each do
        allow(controller).to receive(:require_signature).and_return(true)
      end

      after :each do
        allow(controller).to receive(:require_signature).and_call_original
      end

      it 'responds with an error message' do
        bba = create :black_box_assessment
        expect do
          create_results bba.to_param, results: [attributes_for(:result, status: nil)]
        end.to change(Result, :count).by(1)
        error_message = 'Security test post values not valid: Validation failed: Status ' \
                        "can't be blank, Nid can only contain alphanumeric, dashes, and underscores"
        expect(Result.last.description).to eq error_message
        expect(response.status).to eq 200
      end

      it 'creates an error result when results are missing' do
        bba = create :white_box_assessment
        expect do
          create_results bba.to_param, results: []
        end.to raise_error ActionController::ParameterMissing
        error_message = 'Security test results are missing critical information:' \
                        ' param is missing or the value is empty: results'
        expect(Result.last.description).to eq error_message
      end
    end
  end
end
