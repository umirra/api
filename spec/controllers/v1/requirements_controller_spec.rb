# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::RequirementsController, type: :controller do
  include NoradControllerTestHelpers

  def create_payload(name, description)
    { requirement_group_id: @req_group.to_param, requirement: { name: name, description: description } }
  end

  before :each do
    @req_group = create :requirement_group
  end

  describe 'GET #show' do
    before :each do
      @requirement = create :requirement
    end

    it 'allows all users to read a requirement' do
      expect(@_current_user.has_role?(:requirement_group_admin, @requirement.requirement_group)).to eq(false)
      norad_get :show, id: @requirement.to_param
      expect(response.status).to eq(200)
      expect(response).to match_response_schema('requirement')
    end
  end

  describe 'POST #create' do
    context 'as requirement_group admin,' do
      before :each do
        @_current_user.add_role :requirement_group_admin, @req_group
      end

      it 'creates a new requirement' do
        expect(@_current_user.has_role?(:requirement_group_admin, @req_group)).to eq(true)
        payload = create_payload('myname1', 'My Description')
        norad_post :create, payload
        expect(response.status).to eq(200)
        expect { norad_post :create, payload }.to change(Requirement, :count).by(1)
      end

      it 'exposes requirement' do
        expect(@_current_user.has_role?(:requirement_group_admin, @req_group)).to eq(true)
        norad_post :create, create_payload('myname2', 'My Description')
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('requirement')
      end
    end

    context 'as not a requirement_group admin,' do
      it 'cannot create a new requirement' do
        expect(@_current_user.has_role?(:requirement_group_admin, @req_group)).to eq(false)
        payload = create_payload('myname3', 'My Description')
        norad_post :create, payload
        expect { norad_post :create, payload }.to change(Requirement, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @req = create :requirement, requirement_group: @req_group
    end

    context 'as requirement_group admin,' do
      before :each do
        @_current_user.add_role :requirement_group_admin, @req_group
      end

      it 'deletes a requirement' do
        expect(@_current_user.has_role?(:requirement_group_admin, @req_group)).to eq(true)
        expect { norad_delete :destroy, id: @req }.to change(@req_group.requirements, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as not a requirement_group admin,' do
      it 'cannot create a new requirement' do
        expect(@_current_user.has_role?(:requirement_group_admin, @req_group)).to eq(false)
        expect { norad_delete :destroy, id: @req }.to change(@req_group.requirements, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      rg_no_privs = create :requirement_group
      rg_with_admin = create(:requirement_group).tap { |rg| @_current_user.add_role :requirement_group_admin, rg }
      @req_no_privs = create :requirement, requirement_group: rg_no_privs
      @req_with_admin = create :requirement, requirement_group: rg_with_admin
    end

    context 'as a requirement group admin' do
      it 'updates the name and description of the requirement' do
        new_name = @req_with_admin.name + SecureRandom.hex
        norad_put :update, id: @req_with_admin.to_param, requirement: { name: new_name }
        expect(@req_with_admin.reload.name).to eq(new_name)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('requirement')
      end
    end

    context 'as an unprivileged user' do
      it 'cannot update the name and description of the requirement' do
        new_name = @req_no_privs.name + SecureRandom.hex
        norad_put :update, id: @req_no_privs.to_param, requirement: { name: new_name }
        expect(@req_no_privs.reload.name).to_not eq(new_name)
        expect(response.status).to eq(403)
      end
    end
  end
end
