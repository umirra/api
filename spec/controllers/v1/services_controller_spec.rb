# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ServicesController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @machine = create :machine
      @service = create :service, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the services associated with a machine' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
      end
    end

    context 'as an organization reader' do
      it 'returns the services associated with a machine' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of services for a machine' do
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @machine = create :machine
      @service = create :service, machine: @machine
    end

    context 'as an organization admin' do
      it 'returns the details of a service' do
        @_current_user.add_role :organization_admin, @machine.organization
        norad_get :show, id: @service.id
        expect(response.status).to eq(200)
      end
    end

    context 'as an organization reader' do
      it 'read the details of a specific service' do
        @_current_user.add_role :organization_reader, @machine.organization
        norad_get :show, id: @service.id
        expect(response.status).to eq(200)
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific service' do
        norad_get :show, id: @service.id
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      create :common_service_type, name: 'ftp', port: 21, transport_protocol: 'tcp'
      create :common_service_type, name: 'https', port: 443, transport_protocol: 'tcp'
    end

    let(:service_params) { { service: attributes_for(:service, machine: nil) } }

    context 'as an organization admin' do
      it 'creates a new service for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        create_params = service_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.services, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        Service.find(response_body['response']['id'])
      end

      it 'creates a new service with a common service type for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        create_params = service_params.merge(machine_id: @machine1.to_param)
        create_params[:service][:allow_brute_force] = true
        create_params[:service][:common_service_type] = { name: 'ftp', port: 21, protocol: 'tcp' }
        expect { norad_post :create, create_params }.to change(@machine1.services, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(JSON.parse(response.body)['response']['common_service_type']).not_to eq nil
        Service.find(response_body['response']['id'])
      end

      it 'creates a new web application service for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        create_params = service_params.merge(machine_id: @machine1.to_param)
        create_params[:service][:type] = 'WebApplicationService'
        create_params[:service][:common_service_type] = { name: 'https', port: 443, protocol: 'tcp' }
        expect { norad_post :create, create_params }.to change(@machine1.services, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(JSON.parse(response.body)['response']['common_service_type']).not_to eq nil
        Service.find(response_body['response']['id'])
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new service for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        create_params = service_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new service for a machine' do
        @_current_user.add_role :organization_admin, @machine2.organization
        create_params = service_params.merge(machine_id: @machine1.to_param)
        expect { norad_post :create, create_params }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @service1 = create :service, machine: @machine1
      @service2 = create :service, machine: @machine2
      @new_name = "new_name_#{rand}"
    end

    let(:service_params) { { service: attributes_for(:service, machine: nil, name: @new_name) } }

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @machine1.organization
      end

      it 'updates a service for a machine' do
        expect(@service1.name).to_not eq(@new_name)
        update_params = service_params.merge(id: @service1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(@service1.reload.name).to eq(@new_name)
      end

      it 'does not allow the type to change' do
        expect(@service1.type).to eq('GenericService')
        update_params = { service: { type: 'SshService' } }.merge(id: @service1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service')
        expect(@service1.reload.type).to eq('GenericService')
      end
    end

    context 'as an organization reader' do
      it 'cannot update a service for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        update_params = service_params.merge(id: @service1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot update a machine in organization' do
        @_current_user.add_role :organization_admin, @machine2.organization
        update_params = service_params.merge(id: @service1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @service1 = create :service, machine: @machine1
      @service2 = create :service, machine: @machine2
    end

    context 'as an organization admin' do
      it 'deletes a service for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete a service for a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a service for a machine' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect { norad_delete :destroy, id: @service1 }.to change(@machine1.services, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
