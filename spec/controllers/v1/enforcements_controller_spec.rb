# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::EnforcementsController, type: :controller do
  include NoradControllerTestHelpers

  describe 'GET #index' do
    before :each do
      @org = create :organization
      req_group = create :requirement_group, name: 'PSB'
      @org.enforcements << build(:enforcement, requirement_group: req_group)
      @enforcement = @org.enforcements.first
    end

    context 'as an organization admin' do
      it 'returns the enforcements for an organization' do
        @_current_user.add_role :organization_admin, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('enforcements')
      end
    end

    context 'as an organization reader' do
      it 'returns the enforcements for an organization' do
        @_current_user.add_role :organization_reader, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('enforcements')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of enforcements for the organization' do
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      @req_group = create :requirement_group, name: 'PSB'
    end

    let(:enforcement_params) { { enforcement: { requirement_group_id: @req_group.id } } }

    context 'as an organization admin' do
      it 'creates new enforcement for in organization' do
        @_current_user.add_role :organization_admin, @org1
        create_params = enforcement_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.enforcements, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('enforcement')
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new enforcement for in organization' do
        @_current_user.add_role :organization_reader, @org1
        create_params = enforcement_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.enforcements, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new enforcement for in organization' do
        org2 = create :organization
        @_current_user.add_role :organization_reader, org2
        create_params = enforcement_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.enforcements, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @org1 = create :organization
      req_group = create :requirement_group, name: 'PSB'
      @org1.enforcements << build(:enforcement, requirement_group: req_group)
      @enforcement = @org1.enforcements.first
    end

    context 'as an organization admin' do
      it 'deletes an enforcement for an organization' do
        @_current_user.add_role :organization_admin, @org1
        expect { norad_delete :destroy, id: @enforcement }.to change(@org1.enforcements, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete an enforcement for an organization' do
        @_current_user.add_role :organization_reader, @org1
        expect { norad_delete :destroy, id: @enforcement }.to change(@org1.enforcements, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete an enforcement for an organization' do
        org2 = create :organization
        @_current_user.add_role :organization_admin, org2
        expect { norad_delete :destroy, id: @enforcement }.to change(@org1.enforcements, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
