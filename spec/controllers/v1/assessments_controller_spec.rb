# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::AssessmentsController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @org1 = create :organization
    @org2 = create :organization
    @machine = create :machine, organization: @org1
  end

  describe 'GET #index' do
    context 'as an admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'exposes all assessments for a machine' do
        assessments = Array.new(2) do
          create :white_box_assessment, machine: @machine
        end
        norad_get :index, machine_id: @machine.to_param, limit: 'all'
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
        expect(response_body['response'].size).to eq(assessments.size)
      end
    end

    context 'as an admin from another organization' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'and fail' do
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'as a reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'exposes all assessments for a machine' do
        assessments = Array.new(2) do
          create :white_box_assessment, machine: @machine
        end
        norad_get :index, machine_id: @machine.to_param, limit: 'all'
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
        expect(response_body['response'].size).to eq(assessments.size)
      end
    end

    context 'as a reader from another organization' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'and fail' do
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #latest' do
    before :each do
      @wba = create :white_box_assessment, machine: @machine
    end

    context 'as an admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
      end

      it 'includes results in the response' do
        Array.new(3) do
          @wba.results << create(:result)
        end
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response_body['response'].first['results']).to_not be_empty
        expect(response).to match_response_schema('assessments')
      end
    end

    context 'as an admin from another organization' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'and fail' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'as a reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
      end
    end

    context 'as a reader from another organization' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @wba = create :white_box_assessment, machine: @machine
    end

    context 'as an admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessment')
      end
    end

    context 'as an admin from another organization' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'and fail' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'as a reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessment')
      end
    end

    context 'as a reader from another organization' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(403)
      end
    end
  end
end
