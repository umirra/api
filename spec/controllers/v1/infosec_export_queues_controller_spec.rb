# frozen_string_literal: true

require 'rails_helper'
require 'controllers/v1/shared_examples/result_export_queue'

RSpec.describe V1::InfosecExportQueuesController, type: :controller do
  let(:org) { create :organization }

  let(:valid_create_attributes) do
    valid_update_attributes
  end

  let(:invalid_create_attributes) do
    invalid_update_attributes
  end

  let(:valid_update_attributes) do
    { infosec_export_queue: attributes_for(:infosec_export_queue) }
  end

  let(:invalid_update_attributes) do
    { infosec_export_queue: attributes_for(:infosec_export_queue) }
  end

  let(:project_object) { create :infosec_export_queue, organization: org }
  let(:schema) { 'infosec_export_queue' }

  it_behaves_like 'a Result Export Queue Controller'
end
