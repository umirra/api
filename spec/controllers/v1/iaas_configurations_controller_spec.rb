# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::IaasConfigurationsController, type: :controller do
  include NoradControllerTestHelpers

  describe 'singular resource GET #show' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @iaas_configuration_org1 = create :iaas_configuration, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, organization_id: @org1.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('iaas_configuration')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :show, organization_id: @org1.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, organization_id: @org1.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @iaas_configuration_org1 = create :iaas_configuration, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @iaas_configuration_org1.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('iaas_configuration')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :show, id: @iaas_configuration_org1
        expect(response.status).to eq(403)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific iaas_configuration' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @iaas_configuration_org1
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can create new iaas_configuration for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@org1.iaas_configuration).to be(nil)
        payload = {
          organization_id: @org1.to_param,
          iaas_configuration: attributes_for(:iaas_configuration, provider: 'aws')
        }
        norad_post :create, payload
        expect(@org1.reload.iaas_configuration).not_to be(nil)
        expect(response.status).to eq(200)
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot create new iaas_configuration for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        expect(@org1.reload.iaas_configuration).to be(nil)
        payload = {
          organization_id: @org1.to_param,
          iaas_configuration: attributes_for(:iaas_configuration, provider: 'aws')
        }
        norad_post :create, payload
        expect(@org1.reload.iaas_configuration).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot create new iaas_configuration for organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@org1.reload.iaas_configuration).to be(nil)
        payload = {
          organization_id: @org1.to_param,
          iaas_configuration: attributes_for(:iaas_configuration, provider: 'aws')
        }
        norad_post :create, payload
        expect(@org1.reload.iaas_configuration).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @iaas_configuration_org1 = create :iaas_configuration, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can update an iaas_configuration in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { id: @iaas_configuration_org1.id, iaas_configuration: { key: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(200)
        @iaas_configuration_org1.reload
        expect(@iaas_configuration_org1.key).to eq('changed')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot update an iaas_configuration in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        payload = { id: @iaas_configuration_org1.id, iaas_configuration: { key: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(403)
      end
    end

    context 'an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot update an iaas_configuration in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        payload = { id: @iaas_configuration_org1.id, iaas_configuration: { key: 'changed' } }
        norad_put :update, payload
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @iaas_configuration_org1 = create :iaas_configuration, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can delete an iaas_configuration in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_delete :destroy, id: @iaas_configuration_org1.id
        expect { @iaas_configuration_org1.reload }.to raise_error(ActiveRecord::RecordNotFound)
        expect(response.status).to eq(204)
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot delete an iaas_configuration in organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_delete :destroy, id: @iaas_configuration_org1.id
        expect { @iaas_configuration_org1.reload }.not_to raise_error
        expect(response.status).to eq(403)
      end
    end

    context 'an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'cannot delete an iaas_configuration in this organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_delete :destroy, id: @iaas_configuration_org1.id
        expect { @iaas_configuration_org1.reload }.not_to raise_error
        expect(response.status).to eq(403)
      end
    end
  end
end
