# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  created_by      :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_122abe645c  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :result_export_queue do
    association :organization
    auto_sync false
    type 'InfosecExportQueue'
    created_at DateTime.now.iso8601
    updated_at DateTime.now.iso8601
    created_by { create(:user).uid }

    factory :jira_export_queue, class: 'JiraExportQueue' do
      type 'JiraExportQueue'
      after(:build) do |jp|
        jp.custom_jira_configuration = build(:custom_jira_configuration, jira_export_queue: jp)
      end
    end

    factory :infosec_export_queue, class: 'InfosecExportQueue' do
      type 'InfosecExportQueue'
    end
  end
end
