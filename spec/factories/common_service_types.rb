# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: common_service_types
#
#  id                 :integer          not null, primary key
#  port               :integer          not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  transport_protocol :integer          default("tcp"), not null
#
# Indexes
#
#  index_common_service_types_on_port  (port)
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :common_service_type do
    port 22
    name 'ssh'
    transport_protocol 'tcp'
  end
end
