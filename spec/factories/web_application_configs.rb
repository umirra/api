# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: web_application_configs
#
#  id                             :integer          not null, primary key
#  auth_type                      :integer          default("unauthenticated"), not null
#  url_blacklist                  :string
#  starting_page_path             :string           default("/"), not null
#  login_form_username_field_name :string
#  login_form_password_field_name :string
#  service_id                     :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#
# Indexes
#
#  index_web_application_configs_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_048e8c2543  (service_id => services.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :web_application_config do
    auth_type :unauthenticated
    url_blacklist ''
    starting_page_path '/'
    web_application_service
  end
end
