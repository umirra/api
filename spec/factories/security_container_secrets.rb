# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_container_secrets
#
#  id               :integer          not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  secret_encrypted :string
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :security_container_secret do
  end
end
