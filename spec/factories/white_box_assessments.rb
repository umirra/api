# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: assessments
#
#  id                           :integer          not null, primary key
#  identifier                   :string           not null
#  machine_id                   :integer
#  state                        :integer          default("pending_scheduling"), not null
#  title                        :string
#  description                  :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  security_container_id        :integer
#  security_container_secret_id :integer
#  docker_command_id            :integer
#  state_transition_time        :datetime         not null
#  service_id                   :integer
#  type                         :string           not null
#
# Indexes
#
#  index_assessments_on_docker_command_id             (docker_command_id)
#  index_assessments_on_identifier                    (identifier) UNIQUE
#  index_assessments_on_machine_id                    (machine_id)
#  index_assessments_on_security_container_id         (security_container_id)
#  index_assessments_on_security_container_secret_id  (security_container_secret_id)
#  index_assessments_on_service_id                    (service_id)
#
# Foreign Keys
#
#  fk_rails_1e655192d7  (docker_command_id => docker_commands.id) ON DELETE => cascade
#  fk_rails_85b2eca077  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_b6420ab89f  (security_container_secret_id => security_container_secrets.id) ON DELETE => cascade
#  fk_rails_d67eacf5ca  (service_id => services.id) ON DELETE => cascade
#  fk_rails_f44c8209a6  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :white_box_assessment, aliases: [:wba] do
    sequence(:title) { |n| "norad-registry.cisco.com:5000/serverspec:#{n}" }
    identifier { ::OpenSSL::Digest::SHA1.hexdigest("#{::Kernel.rand}#{Time.now.to_f}") }
    machine
    security_container { FactoryGirl.create(:whitebox_container) }
    docker_command { FactoryGirl.build(:docker_command, machine: machine, containers: [security_container.name]) }
    type 'WhiteBoxAssessment'
    service
    security_container_secret
  end
end
