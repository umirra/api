# frozen_string_literal: true

require 'rails_helper'

describe DockerRelayAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
  end

  context 'when authorizing at the instance level' do
    before :each do
      @docker_relay = create :docker_relay
      @docker_relay.update_column(:verified, true)
      organization = @docker_relay.organization
      @admin_user.add_role :organization_admin, organization
    end

    it 'lets admin users update' do
      expect(@docker_relay.authorizer).to be_updatable_by(@admin_user)
      expect(@docker_relay.authorizer).to_not be_updatable_by(@unprivileged_user)
    end

    it 'lets admin users destroy' do
      expect(@docker_relay.authorizer).to be_deletable_by(@admin_user)
      expect(@docker_relay.authorizer).to_not be_deletable_by(@unprivileged_user)
    end

    it 'lets admin users read' do
      expect(@docker_relay.authorizer).to be_readable_by(@admin_user)
      expect(@docker_relay.authorizer).to_not be_readable_by(@unprivileged_user)
    end
  end

  context 'when authorizing at the class level' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @admin_user.add_role :organization_admin, @org1
    end

    it 'allows admin users to read relays for an organization' do
      expect(DockerRelay.authorizer).to be_readable_by(@admin_user, in: @org1)
      expect(DockerRelay.authorizer).to_not be_readable_by(@unprivileged_user, in: @org1)
      expect(DockerRelay.authorizer).to_not be_readable_by(@admin_user, in: @org2)
    end

    it 'requires that an organization be passed to the authorization method' do
      expect(DockerRelay.authorizer).to be_readable_by(@admin_user, in: @org1)
      expect(DockerRelay.authorizer).to_not be_readable_by(@admin_user, in: nil)
      expect(DockerRelay.authorizer).to_not be_readable_by(@admin_user, in: [])
      expect(DockerRelay.authorizer).to_not be_readable_by(@admin_user, some_key: 123)
    end
  end
end
