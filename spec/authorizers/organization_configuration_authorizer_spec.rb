# frozen_string_literal: true

require 'rails_helper'

describe OrganizationConfigurationAuthorizer, type: :authorizer do
  context 'when authorizing at the instance level' do
    before :each do
      @admin_user = create :user
      @unprivileged_user = create :user
      organization = create :organization
      @config = organization.configuration
      @admin_user.add_role :organization_admin, organization
    end

    it 'lets admin users update' do
      expect(@config.authorizer).to be_updatable_by(@admin_user)
      expect(@config.authorizer).to_not be_updatable_by(@unprivileged_user)
    end

    it 'lets admin and reader users read' do
      expect(@config.authorizer).to be_readable_by(@admin_user)
      expect(@config.authorizer).to_not be_readable_by(@unprivileged_user)
    end
  end
end
