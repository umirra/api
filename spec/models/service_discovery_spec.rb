# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: service_discoveries
#
#  id                  :integer          not null, primary key
#  machine_id          :integer
#  container_secret_id :integer
#  state               :integer
#  error_message       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_service_discoveries_on_container_secret_id  (container_secret_id)
#  index_service_discoveries_on_in_progress_state    (machine_id,state) UNIQUE
#  index_service_discoveries_on_machine_id           (machine_id)
#  index_service_discoveries_on_pending_state        (machine_id,state) UNIQUE
#
# Foreign Keys
#
#  fk_rails_92a60df35e  (container_secret_id => security_container_secrets.id) ON DELETE => cascade
#  fk_rails_fc86baf86f  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'aasm/rspec'

RSpec.describe ServiceDiscovery, type: :model do
  context 'when validating' do
    it { validate_presence_of(:machine) }

    it 'validates the absence of error_message unless the state is failed' do
      p = build :service_discovery, state: ServiceDiscovery::PENDING_STATE, error_message: 'abc'
      ip = build :service_discovery, state: ServiceDiscovery::PENDING_STATE, error_message: 'abc'
      c = build :service_discovery, state: 2, error_message: 'abc'
      f = build :service_discovery, state: 3, error_message: 'abc'
      expect(p.valid?).to be(false)
      expect(p.errors.messages[:error_message]).to include('must be blank')
      expect(ip.valid?).to be(false)
      expect(ip.errors.messages[:error_message]).to include('must be blank')
      expect(c.valid?).to be(false)
      expect(c.errors.messages[:error_message]).to include('must be blank')
      expect(f.valid?).to be(true)
    end

    it 'validates the presence of error_message if the state is failed' do
      p = build :service_discovery, state: ServiceDiscovery::PENDING_STATE, error_message: ''
      ip = build :service_discovery, state: ServiceDiscovery::PENDING_STATE, error_message: ''
      c = build :service_discovery, state: 2, error_message: ''
      f = build :service_discovery, state: 3, error_message: ''
      expect(p.valid?).to be(true)
      expect(ip.valid?).to be(true)
      expect(c.valid?).to be(true)
      expect(f.valid?).to be(false)
      expect(f.errors.messages[:error_message]).to include("can't be blank")
    end
  end

  context 'database constraints' do
    before :each do
      @machine = create :machine
    end

    it 'only allows one pending discovery per machine' do
      expect do
        create :service_discovery, state: ServiceDiscovery::PENDING_STATE, machine: @machine
      end.to_not raise_error

      expect do
        create :service_discovery, state: ServiceDiscovery::PENDING_STATE, machine: @machine
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'only allows one in progress discovery per machine' do
      expect do
        create :service_discovery, state: ServiceDiscovery::IN_PROGRESS_STATE, machine: @machine
      end.to_not raise_error

      expect do
        create :service_discovery, state: ServiceDiscovery::IN_PROGRESS_STATE, machine: @machine
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

  context 'when executing callbacks' do
    context 'before validation' do
      it 'creates a shared secret' do
        discovery = build :service_discovery
        expect(discovery.container_secret).to be(nil)
        discovery.save!
        expect(discovery.container_secret).to_not be(nil)
      end
    end

    context 'after commit on create', with_after_commit: true do
      it 'schedules a service discovery job' do
        discovery = build :service_discovery
        expect(discovery).to receive(:schedule_discovery).once
        discovery.save!
      end
    end
  end

  context 'state machine' do
    before :each do
      @discovery = create :service_discovery
    end

    it 'only allows a transition to in progress if the discovery is pending' do
      expect(@discovery).to transition_from(:pending).to(:in_progress).on_event(:start)
    end

    it 'only allows a transition to complete if the discovery is in progress' do
      expect(@discovery).to transition_from(:in_progress).to(:completed).on_event(:complete)
    end

    it 'only allows a transition to failed if the discovery is in progress or pending' do
      expect(@discovery).to transition_from(:in_progress).to(:failed).on_event(:fail)
      expect(@discovery).to transition_from(:pending).to(:failed).on_event(:fail)
    end
  end

  describe 'class methods' do
    it 'returns service discoveries that failed to report results via .stale' do
      create(:ip_service_discovery, updated_at: 1.hour.ago)
      crashed_sd = create(:ip_service_discovery, updated_at: 25.hours.ago)
      create(:service_discovery, updated_at: 25.hours.ago)
      create(:completed_service_discovery, updated_at: 25.hours.ago)
      create(:failed_service_discovery, updated_at: 25.hours.ago)
      expect(described_class.stale.first.id).to eq(crashed_sd.id)
    end
  end
end
