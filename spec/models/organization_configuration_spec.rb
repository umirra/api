# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_configurations
#
#  id                         :integer          not null, primary key
#  organization_id            :integer          not null
#  auto_approve_docker_relays :boolean          default(FALSE), not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  use_relay_ssh_key          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_organization_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_2555d2097f  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe OrganizationConfiguration, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:organization) }
  end

  context 'when maintaining associations' do
    it { should belong_to(:organization) }
  end

  context 'when setting default values' do
    before :each do
      @org = build :organization, configuration: nil
      allow(@org).to receive(:build_configuration) { nil }
      @org.save!
    end

    it 'should set auto_approve_docker_relays to false' do
      config = OrganizationConfiguration.create!(organization_id: @org.id)
      expect(config.auto_approve_docker_relays).to eq false
    end

    it 'should set use_relay_ssh_key to false' do
      config = OrganizationConfiguration.create!(organization_id: @org.id)
      expect(config.use_relay_ssh_key).to eq false
    end
  end

  context 'when updating' do
    before :each do
      @org = build :organization, configuration: nil
      relay = create :docker_relay, organization: @org
      relay.verified = true
      relay.save!
      @org.save!
      security_container = create :security_container, test_types: [:authenticated]
      create :security_container_config, configurable: @org, security_container: security_container
      @machine = create :machine, organization: @org
    end

    context 'with ssh_key_pair' do
      context 'with ssh_key_pair_assignment' do
        before :each do
          create :ssh_key_pair_assignment, machine: @machine
        end

        it 'using relay ssh key' do
          @org.configuration.update(use_relay_ssh_key: true)
          expect(@org.organization_errors.map(&:class)).to eq []
        end

        it 'not using relay ssh key' do
          @org.configuration.update(use_relay_ssh_key: false)
          expect(@org.organization_errors.map(&:class)).to eq []
        end
      end

      context 'without ssh_key_pair_assignment' do
        before :each do
          create :ssh_key_pair, organization: @org
        end

        it 'using relay ssh key' do
          @org.configuration.update(use_relay_ssh_key: true)
          expect(@org.organization_errors.map(&:class)).to eq []
        end

        it 'not using relay ssh key' do
          @org.configuration.update(use_relay_ssh_key: false)
          expect(@org.organization_errors.map(&:class)).to eq [NoSshKeyPairAssignmentError]
        end
      end
    end

    context 'without ssh_key_pair' do
      it 'using relay ssh key' do
        @org.configuration.update(use_relay_ssh_key: true)
        expect(@org.organization_errors.map(&:class)).to eq []
      end

      it 'not using relay ssh key' do
        @org.configuration.update(use_relay_ssh_key: false)
        expect(@org.organization_errors.map(&:class)).to eq [NoSshKeyPairError, NoSshKeyPairAssignmentError]
      end
    end
  end
end
