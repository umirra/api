# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: notification_channels
#
#  id              :integer          not null, primary key
#  enabled         :boolean          default(TRUE)
#  event           :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_notification_channels_on_event                      (event)
#  index_notification_channels_on_organization_id_and_event  (organization_id,event) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe NotificationChannel, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:organization) }
    it { should validate_presence_of(:event) }
    it { should validate_uniqueness_of(:event).case_insensitive.scoped_to(:organization_id) }
  end

  context 'when maintaining associations' do
    it { should belong_to(:organization) }
  end
end
