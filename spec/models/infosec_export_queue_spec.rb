# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id  (organization_id)
#  index_result_export_queues_on_type             (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/result_export_queue'

RSpec.describe InfosecExportQueue, type: :model do
  it_behaves_like 'a Result Export Queue' do
    let(:amqp_queue_name) { 'result_export_queue.cisco_infosec.21f4a6b84612b4a40d5974dcdd9e4106' }
  end
end
