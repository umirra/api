# frozen_string_literal: true

# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: services
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  description            :text
#  port                   :integer          not null
#  port_type              :integer          default("tcp"), not null
#  encryption_type        :integer          default("cleartext"), not null
#  machine_id             :integer
#  type                   :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  allow_brute_force      :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  discovered             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_common_service_type_id  (common_service_type_id)
#  index_services_on_machine_id              (machine_id)
#  index_services_on_machine_id_and_port     (machine_id,port) UNIQUE
#  index_services_on_type                    (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (common_service_type_id => common_service_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe WebApplicationService, type: :model do
  context 'when validating' do
    it 'ensures the encryption type is ssl or disabled' do
      was = build :web_application_service
      was.encryption_type = :ssh
      was.valid?
      expect(was.errors.messages[:encryption_type]).to include('must be SSL or cleartext')
      was.encryption_type = :ssl
      was.valid?
      expect(was.errors.messages[:encryption_type]).to eq([])
      was.encryption_type = :cleartext
      was.valid?
      expect(was.errors.messages[:encryption_type]).to eq([])
    end
  end

  context 'when executing callbacks' do
    context 'before validating' do
      before :each do
        @was = build :web_application_service
      end

      it 'ensures the port type is TCP' do
        @was.port_type = :udp
        @was.valid?
        expect(@was.port_type).to eq('tcp')
      end

      it 'sets the port to 443 if none is set and the encryption type is encrypted' do
        @was.encryption_type = :ssl
        @was.port = nil
        @was.valid?
        expect(@was.port).to eq(443)
      end

      it 'sets the port to 80 if none is set and the encryption type is cleartext' do
        @was.encryption_type = :cleartext
        @was.port = nil
        @was.valid?
        expect(@was.port).to eq(80)
      end
    end

    context 'before saving' do
      it 'is always associated with the http service type for cleartext services' do
        web_app = build :web_application_service, port: 1234, encryption_type: :cleartext
        expect(web_app.common_service_type).to be(nil)
        web_app.save!
        cst = CommonServiceType.find_by(name: 'http', port: '80', transport_protocol: 'tcp')
        expect(web_app.common_service_type).to eq(cst)
      end

      it 'is always associated with the https service type for tls services' do
        web_app = build :web_application_service, port: 1234, encryption_type: :ssl
        expect(web_app.common_service_type).to be(nil)
        web_app.save!
        cst = CommonServiceType.find_by(name: 'https', port: '443', transport_protocol: 'tcp')
        expect(web_app.common_service_type).to eq(cst)
      end
    end
  end

  context 'when maintaining associations' do
    it { should have_one(:web_application_config).with_foreign_key(:service_id) }
  end
end
