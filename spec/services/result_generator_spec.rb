# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultGenerator do
  describe 'instance methods' do
    let(:subject) { described_class.new(create(:assessment)) }

    # Known-good result signatures for errored and timed out results. If these fail, the implementation has changed and
    # all existing errored and timed out result signatures need to be updated for all organizations.
    let(:errored_hash) { '3bf184b1d5a1a7ba78912fb002fb45e4a8cd4bd0364d7bf827f8b8333e6baa28' }
    let(:timed_out_hash) { '1c4365d5afb7f6fd95740c6321150cdadf996c99fb094dbb977b850b83243221' }

    it 'generates an errored result' do
      expect { subject.create_errored_result! }.to change(Result, :count).by(1)
      expect(Result.last.signature).to eq errored_hash
    end

    it 'generates a timed out result' do
      expect { subject.create_timed_out_result! }.to change(Result, :count).by(1)
      expect(Result.last.signature).to eq timed_out_hash
    end
  end
end
