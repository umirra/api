# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultExporter do
  describe 'instance methods' do
    let(:exporter) { described_class.new(export_queue, scan) }
    let(:export_queue) { double }
    let(:scan) { double }

    describe '#publish' do
      before :each do
        allow(exporter).to receive(:results_data).and_return({})
        allow(export_queue).to receive(:amqp_queue_name).and_return('fooqueue')
      end

      it 'asks the Publisher service to publish to the default exchange' do
        expect(Publisher).to receive(:publish).with('', instance_of(Hash), instance_of(String))
        exporter.publish
      end
    end

    describe '#results_data' do
      before :each do
        allow(exporter).to receive(:results).and_return([double, double])
      end

      it 'builds a hash with serialized result data' do
        expect(ResultExporterSerializers::Result).to receive(:new).exactly(2).times
        exporter.results_data
      end
    end
  end
end
