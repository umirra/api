# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotificationMailer, type: :mailer do
  context 'when emailing' do
    before :each do
      @org_admin = create :user
      @org = @org_admin.organizations.first
    end

    context 'valid scans' do
      before :each do
        sc = create(:security_container)
        @dc = create(
          :docker_command,
          commandable: @org,
          assessments: [create(:assessment, security_container: sc)],
          containers: [sc.name],
          started_at: 1.day.ago,
          finished_at: Time.now.utc
        )
      end

      it 'exists in deliveries queue' do
        expect { NotificationMailer.scan_complete(@dc.id).deliver_now } \
          .to change(ActionMailer::Base.deliveries, :count).by(1)
      end

      it 'includes members of org in the bcc field' do
        mail = NotificationMailer.scan_complete(@dc.id)
        expected_recipients = @org_admin.email
        expect(mail.bcc).to include(expected_recipients)
      end

      it 'displays event data in the email body' do
        mail = NotificationMailer.scan_complete(@dc.id)
        expect(mail.body).to include(v1_organization_url(@org).sub(%r{/v1}, ''))
        expect(mail.body).to include(@dc.started_at)
        expect(mail.body).to include(@dc.finished_at)
        expect(mail.body).to include('1 day')
      end
    end

    context 'errored scans' do
      before :each do
        @dc = create(
          :docker_command,
          commandable: @org,
          error_details: 'Test error details'
        )
      end

      it 'displays scan errors' do
        mail = NotificationMailer.scan_complete(@dc.id)
        expect(mail.body).to include('Test error details')
      end
    end

    context 'empty scans' do
      before :each do
        @dc = create(
          :docker_command,
          commandable: @org,
          started_at: 1.day.ago
        )
      end

      it 'notifies user no assessments were completed' do
        mail = NotificationMailer.scan_complete(@dc.id)
        expect(mail.body).to include('Your scan has finished, but no relevant assessments were completed.')
        expect(mail.body).to include(@dc.started_at)
      end
    end
  end
end
