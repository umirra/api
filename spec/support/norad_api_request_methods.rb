# frozen_string_literal: true

module NoradApiRequestMethods
  def norad_get(action, request_params)
    params_is_hash! request_params
    get action, params: request_params
  end

  def norad_post(action, request_params)
    params_is_hash! request_params
    post action, params: request_params
  end

  def norad_delete(action, request_params)
    params_is_hash! request_params
    delete action, params: request_params
  end

  def norad_put(action, request_params)
    params_is_hash! request_params
    put action, params: request_params
  end

  def params_is_hash!(request_params)
    raise ArgumentError, 'Parameters must be a Hash' unless request_params.is_a? Hash
  end
end
