# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ScheduleContainerJob, type: :job do
  include ActiveJob::TestHelper

  it 'is in the swarm_scheduling queue' do
    expect(described_class.new.queue_name).to eq('swarm_scheduling')
  end
end
