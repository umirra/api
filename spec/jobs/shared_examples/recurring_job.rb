# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a Recurring Job' do
  it 'is in the recurring queue' do
    expect(described_class.instance_variable_get(:@queue)).to eq(:recurring)
  end

  it 'sends the job_action message to the class when performing the job' do
    expect(described_class).to receive(:job_action)
    described_class.perform
  end

  it 'responds to the job_action message when sent to the class' do
    expect(described_class.respond_to?(:job_action)).to eq(true)
  end

  it 'notifies Airbrake when a StandardError is raised while performing a job' do
    allow(described_class).to receive(:job_action).and_raise(StandardError)
    expect(described_class).to receive(:notify_airbrake)
    expect { described_class.perform }.to raise_error(StandardError)
  end
end
