# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ScheduleScanJob, type: :job, with_resque_doubled: true do
  include ActiveJob::TestHelper

  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform(@schedule.to_global_id.to_s) }

  it 'is in the swarm_scheduling queue' do
    expect(described_class.instance_variable_get(:@queue)).to eq(:scan_scheduling)
  end

  it 'creates a docker command for a machine' do
    @schedule = create :machine_scan_schedule
    create :security_container_config, configurable: @schedule.scan_target
    expect(AssessmentBuilderJob).to receive(:perform_later).once
    expect do
      perform_enqueued_jobs { job }
    end.to change(@schedule.scan_target.docker_commands, :count).by(1)
  end

  it 'creates a docker command for an organization' do
    @schedule = create :organization_scan_schedule
    create :security_container_config, configurable: @schedule.scan_target
    expect(AssessmentBuilderJob).to receive(:perform_later).once
    expect do
      perform_enqueued_jobs { job }
    end.to change(@schedule.scan_target.docker_commands, :count).by(1)
  end
end
